<?php

	session_start();
 /*$conn = mysqli_connect("localhost", "root", "", "kingdom_project_db");
 
 session_start();

 $msg = "";

    if(isset($_POST['signin'])){
        $username = $_POST['username'];
        $password = $_POST['password'];
        $password = sha1($password);
        $userType = $_POST['userType'];

        $cmd = "SELECT * FROM users WHERE username='$username' AND password='$password' AND userType='$userType'";

        $query = mysqli_query($conn, $cmd);
        $result = mysqli_fetch_array($query);
      
        session_regenerate_id();
        $_SESSION['username'] = $result['username'];
        $_SESSION['userType'] = $result['userType'];
        session_write_close();
        
       
        if($_SESSION['userType'] == "admin"){
            header("location:../AdminPanel/");
        }
        elseif($_SESSION['userType'] == "shop"){
            header("location:../shop/");
        }
        elseif($_SESSION['userType'] == "customer"){
            header("location:../customer/");
        }
        else{
            $msg = "<p>Invalid Username or Password</p>";
        }
    }
*/

	include('../connection_configuration/conn_config.php');

	$msg = "";
	$username = "";
	$password = "";
	$usertype = "";

	if(isset($_POST['signin'])){
		#$username = mysqli_escape_string($conn_config, $_POST['username']);
        $username = $_POST['username'];
        $password = sha1($_POST['password']);
		#$password = mysqli_escape_string($conn_config, sha1($_POST['password'])) ;
		#$password = sha1($password);
		#$usertype = $_POST['userType'];
		
		#$signin_cmd = "SELECT * FROM `users_pool` WHERE username = '$username' AND userType = '$usertype'";
		$signin_cmd = "SELECT * FROM `users_pool` WHERE username = '$username'";
		$signin_query = mysqli_query($conn_cmd, $signin_cmd);
		
		$signin_result = mysqli_fetch_assoc($signin_query);
		
		#echo $signin_result['username'];
		#echo $signin_result['password'];
		#echo $signin_result['userType'];
		
		if($signin_result != 0){
			
			$_SESSION['Username'] = $signin_result['username'];
			$_SESSION['UserType'] = $signin_result['userType'];
			$_SESSION['id'] = $signin_result['pool_id'];
			
			if($_SESSION['Username'] == $signin_result['username'] and $_SESSION['UserType'] == "admin"){
				header("location: ../AdminPanel/");
			}
			elseif($_SESSION['Username'] == $signin_result['username'] and $_SESSION['id'] == $signin_result['pool_id'] and $_SESSION['UserType'] == "customer"){
				header("location: ../customer/");
			}
			elseif($_SESSION['Username'] == $signin_result['username']  and $_SESSION['id'] == $signin_result['pool_id'] and $_SESSION['UserType'] == "shop"){
				header("location: ../shop/");
			}
			elseif($_SESSION['Username'] == $signin_result['username']  and $_SESSION['id'] == $signin_result['pool_id'] and $_SESSION['UserType'] == "agent"){
				header("location: ../agent/");
			}
            elseif($_SESSION['Username'] == $signin_result['username']  and $_SESSION['id'] == $signin_result['pool_id'] and $_SESSION['UserType'] == "agent-shop"){
                header("location: ../agent-shop/");
            }
			else{
				$msg = "<p>Invalid Username or Password or Role</p>";
			}
		}
		
		
	}
	
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kingdom Card</title>
    <link rel="shortcut icon" href="../img/kd_logo.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <style>
        /*body{
            
            background-image: url("../img/1.jpg") no-repeat center cover;
        }*/
    </style>
</head>
<body class="bg-dark" style="background-image: image('../img/1.jpg'); ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 bg-light mt-5 px-0">
                <h3 class="text-center text-light bg-primary p-3">Sign In</h3>

                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" class="p-4">
                    <div class="form-group">
                        <input type="text" name="username" id="" class="form-control form-control-lg" placeholder="Username..." required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" id="" class="form-control form-control-lg" placeholder="Password..." required>
                    </div>
                    <!--<div class="form-group">
                        <label for="userType">I'm a :</label>&nbsp;
                        <input type="radio" name="userType" value="admin" id="" class="custom-radio" required>&nbsp; Admin |&nbsp;
                        <input type="radio" name="userType" value="shop" id="" class="custom-radio" required>&nbsp; Shop |&nbsp;
                        <input type="radio" name="userType" value="customer" id="" class="custom-radio" required>&nbsp; Customer 
                    </div>-->
                    <div class="form-group">
                        <input type="submit" value="Sign In" name="signin" class="btn btn-primary btn-block">
                    </div>
										<div class="form-group text-capitalize text-center">
											<a href="../signup_form/" target="_blank"><p>Sign up for an Account</p></a>
                                            <!-- <a href="../password_reset/" target="_blank"><p>Forgot Password?</p></a> -->
										</div>
                    <h5 class="text-danger text-center"><?php echo $msg;?></h5>
                </form>
            </div>    
        </div>   
    </div>



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>