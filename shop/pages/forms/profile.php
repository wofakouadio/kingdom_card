<?php

	include('../../../connection_configuration/conn_config.php');

	#echo session_status();
	session_start();
	#echo $_SESSION['Username'];
	#echo $_SESSION['UserType'];

	if(!isset($_SESSION['Username']) and !isset($_SESSION['UserType']) and !isset($_SESSION['id'])){
		header("location:../../../login/");
	}

						$shop_ID = "";
						$picture = "";
						$valid_ID = "";
						$username = $_SESSION['Username'];
						$sum = "SELECT * FROM shops_table WHERE vendor_username = '$username'";
						$sum_query = mysqli_query($conn_cmd,$sum);

						while($res = mysqli_fetch_array($sum_query)){
							$shop_ID = $res['shop_ID'];
							$name = $res['shop_name'];
							$vendor_name = $res['vendor_name'];
							$cat = $res['shop_category'];
							$discount = $res['shop_discount'];
							$loc = $res['shop_location'];
							$tel = $res['shop_contact'];
							$tel2 = $res['vendor_contact'];
							$picture = $res['vendor_picture_path'];
							$valid_ID = $res['vendor_ID_path'];
							$username = $res['vendor_username'];
						}
					
						
			
					
					

?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vendor | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="shortcut icon" href="../img/kd_logo.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo $name; ?></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo $name; ?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../../img/shop.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['Username'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../../img/shop.jpg" class="img-thumbnail" alt="User Image">

                <p>
                  <?php echo $_SESSION['Username'];?>
                  <!--<small>Member since Nov. 2012</small>-->
                </p>
              </li>
    
              <!-- Menu Footer-->
              <li class="user-footer">
                <!--<div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>-->
                <div class="text-center">
                  <a href="../../../logout/" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
         <!--<li class="header">MAIN NAVIGATION</li>-->
        <li>
          <a href="../../">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="active">
          <a href="../forms/profile.php">
            <i class="fa fa-user"></i><span>My Profile</span>
          </a>
        </li>
        <li>
          <a href="../tables/sales.php">
            <i class="fa fa-stack-exchange"></i><span>Sales</span>
          </a>
        </li>
        <li>
          <a href="../tables/credit.php">
            <i class="ion ion-cash"></i><span>Credit</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        My Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><a href="#"><i class="fa fa-user"></i>Profile</a></li>
      </ol>
    </section>

       <!-- Main content -->
       <section class="content">
        <div class="row">
          <!-- left column -->
          <!--<div class="col-md-6">
             general form elements 
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Fill Form</h3>
              </div>
               /.box-header 
               form start 
              <form role="form">
                <div class="box-body">
                  <div class="form-group">
                      <label for="exampleInputText1">Name</label>
                      <input type="text" class="form-control" id="exampleInputText1" placeholder="Full Name..." required>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputText2">Date of Birth</label>
                      <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask placeholder="dd/mm/yyyy" required>
                  </div>
                  <div class="form-group">
                      <label for="userType">Gender :</label>&nbsp;
                      <input type="radio" name="gender" value="Male" id="" class="custom-radio" required>&nbsp; Male &nbsp;
                      <input type="radio" name="gender" value="Female" id="" class="custom-radio" required> &nbsp; Female 
                  </div>
                  <div class="form-group">
                      <label for="exampleInputText3">Profession</label>
                      <input type="text" class="form-control" id="exampleInputTex2t" placeholder="Profession..." required>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputText4">Address</label>
                      <input type="text" class="form-control" id="exampleInputText3" placeholder="Address..." required>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputText5">Telephone Number</label>
                      <input type="text" class="form-control" id="exampleInputText4" placeholder="Telephone Number..." required>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputFile1">Upload Picture</label>
                      <input type="file" id="exampleInputFile2" required>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputFile2">Upload Valid ID</label>
                      <input type="file" id="exampleInputFile2" required>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email..." required>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password..." required>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputPassword1">Repeat Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Repeart Password..." required>
                  </div>
                </div>
                 /.box-body -->
  
                <!--<div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>-->
					
					
          <div class="col-md-9">
            <!-- Horizontal Form -->
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">Summary</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="" enctype="multipart/form-data" >
                  <div class="box-body">
                          <div class="form-group">
                                  <label for="exampleInputText1">Shop ID</label>
                                  <input type="text" class="form-control" id="exampleInputText1" value="<?php echo $shop_ID ;?>" readonly>
                              </div>
                      <div class="form-group">
                          <label for="exampleInputText1">Shop Name</label>
                          <input type="text" class="form-control" id="exampleInputText1" value="<?php echo $name; ?>" readonly>
                      </div>
											<div class="form-group">
                          <label for="exampleInputText1">Vendor Name</label>
                          <input type="text" class="form-control" id="exampleInputText1" value="<?php echo $vendor_name; ?>" readonly>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputText1">Category</label>
                          <input type="text" class="form-control" id="exampleInputText1" value="<?php echo $cat; ?>" readonly>
                      </div>
										<div class="form-group">
                          <label for="exampleInputText1">Discount</label>
                          <input type="text" class="form-control" id="exampleInputText1" value="<?php echo $discount; ?>" readonly>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputText4">Address</label>
                          <input type="text" class="form-control" id="exampleInputText3" value="<?php echo $loc; ?>" readonly>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputText5">Telephone Number</label>
                          <input type="text" class="form-control" id="exampleInputText4" value="<?php echo $tel."/".$tel2; ?>" readonly>
                      </div>
                      <div class="col-md-12">
												<div class="col-md-6">
                          <label for="exampleInputFile1">Picture</label>
                          <img src="<?php echo $picture;?>" alt="" class="img img-thumbnail" width="150px">
												</div>
												<div class="col-md-6 form-group">
														<label for="exampleInputFile2">Valid ID</label>
														<img src="<?php echo $valid_ID; ?>" alt="" class="img img-thumbnail" width="150px">
												</div>
											</div>
                      <br><br><div class="form-group">
                              <label for="exampleInputText1">Username</label>
                              <input type="text" class="form-control" id="exampleInputText1" value="<?php echo $username; ?>" readonly>
                          </div>
                  </div>
                  <!-- /.box-body -->
      
                  <!--<div class="box-footer">
                      <button type="submit" class="btn btn-success">Save</button>
                  </div>-->
              </form>
          </div>
            <!-- /.box -->
            <!-- general form elements disabled -->
            
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </section>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    
    <div class="text-center">
      <strong>Copyright &copy; 2020 <a target="_blank" href="#">Kingdom Dynasty Limited</a>.</strong> All rights
    reserved.
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../bower_components/raphael/raphael.min.js"></script>
<script src="../bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../bower_components/moment/min/moment.min.js"></script>
<script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>
