<?php

	include('../../../connection_configuration/conn_config.php');

	#echo session_status();
	session_start();
	#echo $_SESSION['Username'];
	#echo $_SESSION['UserType'];

	if(!isset($_SESSION['Username']) and !isset($_SESSION['UserType']) and !isset($_SESSION['id'])){
		header("location:../login/");
	}


      $username = $_SESSION['Username'];
      $sum = "SELECT * FROM shops_table WHERE vendor_username = '$username'";
      $sum_query = mysqli_query($conn_cmd,$sum);

      while($res = mysqli_fetch_array($sum_query)){
        $shop_ID = $res['shop_ID'];
        $name = $res['shop_name'];
        $vendor_name = $res['vendor_name'];
        $cat = $res['shop_category'];
        $discount = $res['shop_discount'];
        $loc = $res['shop_location'];
        $tel = $res['shop_contact'];
        $tel2 = $res['vendor_contact'];
        $picture = $res['vendor_picture_path'];
        $valid_ID = $res['vendor_ID_path'];
        $username = $res['vendor_username'];
      }


?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vendor | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="shortcut icon" href="../img/kd_logo.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo $name; ?></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo $name; ?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../../img/shop.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['Username'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../../img/shop.jpg" class="img-thumbnail" alt="User Image">

                <p>
                <?php echo $_SESSION['Username'];?>
                  <!--<small>Member since Nov. 2012</small>-->
                </p>
              </li>
    
              <!-- Menu Footer-->
              <li class="user-footer">
                <!--<div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>-->
                <div class="text-center">
                  <a href="../../../logout/" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
         <!--<li class="header">MAIN NAVIGATION</li>-->
        <li>
          <a href="../../">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="../forms/profile.php">
            <i class="fa fa-user"></i><span>My Profile</span>
          </a>
        </li>
        <li class="active">
          <a href="../tables/sales.php">
            <i class="fa fa-stack-exchange"></i><span>Sales</span>
          </a>
        </li>
        <li>
          <a href="../tables/credit.php">
            <i class="ion ion-cash"></i><span>Credit</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sales Table
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><a href=""><i class="fa fa-stack-exchange"></i>Sales</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <!--/.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Sale ID</th>
                  <th>Customer ID</th>
									<th>Customer Name</th>
                  <th>Description</th>
									<th>Discount</th>
									<th>Date</th>
                </tr>
                </thead>
                <tbody>
									<?php
										$id = $_SESSION['id'];
									$cmd = "SELECT sales_table.sale_ID, sales_table.customer_ID, sales_table.customer_name, sales_table.description, shops_table.shop_discount, sales_table.date FROM sales_table JOIN shops_table WHERE sales_table.shop_ID = shops_table.shop_ID AND shops_table.shop_ID = '$id'";
									$query = mysqli_query($conn_cmd, $cmd);
									
									while($res = mysqli_fetch_array($query)){
										$sid = $res['sale_ID'];
										$scustid = $res['customer_ID'];
										$scustname = $res['customer_name'];
										$sdes = $res['description'];
										$sdis = $res['shop_discount'];
										$sdate = $res['date'];
										
										echo "<tr>";
											echo "<td>".$sid."</td>";
											echo "<td>".$scustid."</td>";
											echo "<td>".$scustname."</td>";
											echo "<td>".$sdes."</td>";
										echo "<td>".$sdis."</td>";
										echo "<td>".$sdate."</td>";
										echo "</tr>";
									}
									?>
									
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Sale ID</th>
                  <th>Customer ID</th>
									<th>Customer Name</th>
                  <th>Description</th>
									<th>Discount</th>
									<th>Date</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!--<div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>-->
    <div class="text-center">
      <strong>Copyright &copy; 2020 <a target="_blank" href="#">Kingdom Dynasty Limited</a>.</strong> All rights
    reserved.
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
