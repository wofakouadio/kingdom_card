<?php
    session_start();

    if(!isset($_SESSION['username']) || $_SESSION['userType'] != "shop"){
        header("location:../login/");
    }
?>

<h1>Hello : <?php  echo $_SESSION['username']; ?></h1>
<h2>You are an : <?= $_SESSION['userType'] ?></h2>
<a href="../logout/">Logout</a>