<?php

    #header('Content-Type: image/png');
   #require "../AdminPanel/qrcodes/vendor/autoload.php";
   include ('../phpqrcode/qrlib.php');
	include ('../connection_configuration/conn_config.php');

   #$qr = new endroid\qr-code\qr-code();

   #$qr = setText("Hello Worlds");
  # $qr = setSize(500);
   #$qr = setPadding(10);

   #$qr = render();
	#$cid = "";
	if(isset($_GET['q'])){



		$username = base64_decode($_GET['q']);

		$query = "SELECT * FROM customers_table WHERE customer_username = '$username'";
		$exe_query = mysqli_query($conn_cmd,$query);

		while($res = mysqli_fetch_array($exe_query)){
			$cid = $res['customer_ID'];
			$cusername = $res['customer_username'];
		}
  }
  
  function get_membershipcard_via_qrcode(){
    $url = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    $validate_url = str_replace("&", "&amp", $url);
    return $validate_url;
  }

  $url = get_membershipcard_via_qrcode();

  $qrcode_dir = "./";
  $filename = $cusername;
  QRcode::png($url, $qrcode_dir . "" . $filename . ".png", QR_ECLEVEL_L, 5);

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>K Card | Membership Card</title>
    <link rel="shortcut icon" href="kd_logo@144.png">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js" integrity="sha256-my/qJggBjG+JoaR9MUSkYM+EpxVkxZRNn3KODs+el74=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <style>
        body{
          margin :0;
        }

        .kd_card{
          width: 70%;
           /*background-color: blue;*/
           position: relative;
        }

        .kd_card .card{
           width: 90%;

        }

        .kd_card .qr{
          width: 175px;
          height: 170px;
          position: absolute;
          top: 9%;
          left: 65%;
        }

        .idnumber{
          letter-spacing: 17px;
          font-size: 30pt;
          text-align: center;
        }

        .kd_card .idnumber{
          background-color: white;
          width: 520px;
          height: 50px;
          position: absolute;
          top: 60%;
          left: 105px;
        }






    </style>
</head>
<body class="bg-dark">
  <div class="container">
    <div class="row justify-content-center">
      <!-- <div class=" col-lg-12 mt-2 px-0 card-body card-danger"> -->
        <div class="kd_card" id="html-content-holder">
          <img src="sesdw.jpg" alt="kingdom_card"  class="img-thumbnail card">
          <img src="<?php echo $cusername;?>.png" alt="<?php echo $cusername;?>.png"  class="img-thumbnail qr" >
          <p class="text-center idnumber" id="member_id"><strong><?php echo $cid;?></strong></p>
        </div>
      </div>
      <div class="row justify-content-center kd_card text-center">
        <input type="button" id="btn-Preview-Image" value="Preview" class="btn btn-info">&nbsp;
        <a href="#" id="btn-Convert-Html2Image" class="btn btn-success">Download</a>
    </div>

    <br><h3 class="text-center">Preview:</h3><br>

    <div class="row justify-content-center kd_card text-center">
      <div id="previewImage" class="kd_card"></div>
    </div>

  </div>

  <script>
    $(document).ready(function(){
      var element = $("#html-content-holder");
      var getCanvas;
      var name = "mc-<?php echo $cusername;?>.png";

      $("#btn-Preview-Image").on('click', function(){
        html2canvas(element, {
          onrendered: function(canvas){
            $("#previewImage").append(canvas);
            getCanvas = canvas;
          }
        });
      });

      $("#btn-Convert-Html2Image").on('click', function(){
        var imageData = getCanvas.toDataURL("image/png");

        //Now browser starts downloading
        //it instead of just showing it

        var newData = imageData.replace(/^data:image\/png/, "data:application/octet-stream");

        $("#btn-Convert-Html2Image").attr("download", name).attr("href", newData);
      });
    });
  </script>
 


<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
</body>
</html>
