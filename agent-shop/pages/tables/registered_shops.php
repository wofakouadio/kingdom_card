<?php

	include('../../../connection_configuration/conn_config.php');

	#echo session_status();
	session_start();
	#echo $_SESSION['Username'];
	#echo $_SESSION['UserType'];

	if(!isset($_SESSION['Username']) and !isset($_SESSION['UserType']) and !isset($_SESSION['id'])){
		header("location:../../../login/");
	}
    
    
  $agent_id = $_SESSION['id'];
  $picture = "";
  $valid_id = "";
  $username = $_SESSION['Username'];
  
  $sum = "SELECT * FROM agents_shops WHERE username = '$username'";
  $sum_query = mysqli_query($conn_cmd,$sum);

  while($res = mysqli_fetch_array($sum_query)){
    $agent_id = $res['agent_ID'];
    $name = $res['name'];
    $gender = $res['gender'];
    $address = $res['address'];
    $profession = $res['profession'];
    $tel = $res['contact'];
    $picture = $res['agent_picture'];
    $valid_id = $res['agent_valid_id'];
    $mail = $res['email'];
    $username = $res['username'];
    
  } 

?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Agent - Shop | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="shortcut icon" href="../img/kd_logo.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo $name; ?></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo $name; ?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="../../../pictures/<?php echo $picture;?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['Username'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
              <img src="../../../pictures/<?php echo $picture;?>" class="img-thumbnail" alt="User Image">

                <p>
                  <?php echo $_SESSION['Username'];?>
                  <!--<small>Member since Nov. 2012</small>-->
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="text-center">
                  <a href="../../../logout/" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
   <!-- Left side column. contains the logo and sidebar -->
   <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
         <!--<li class="header">MAIN NAVIGATION</li>-->
        <li>
          <a href="../../"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        <li>
          <a href="../forms/profile.php"><i class="fa fa-user"></i><span>My Profile</span></a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Shops</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../forms/reg_shops.php"><i class="ion ion-bag"></i>Register</a></li>
            <li class="active"><a href="../tables/registered_shops.php"><i class="fa fa-building"></i><span>View</span></a></li>
          </ul>
        </li>
        <li><a href="../tables/commission.php"><i class="fa fa-money"></i><span>Commission</span></a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Shops Table
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-building"></i> Shops</a></li>
        <li class="active"><a href="">Overview</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <!--/.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Shop Name</th>
                  <th>Shop Contact</th>
                  <th>Vendor Name</th>
                  <th>Vendor Contact</th>
                  <th>Location</th>
									<th>Email</th>
                  <th>Category</th>
                </tr>
                </thead>
                <tbody>
                <?php
									$agent_id = $_SESSION['id'];
									$cmd = "SELECT shops_table.shop_ID, shops_table.shop_name, shops_table.vendor_name, shops_table.shop_contact, shops_table.vendor_contact, shops_table.shop_location, shops_table.shop_category, shops_table.vendor_email FROM shops_table JOIN agents_shops WHERE shops_table.agent_shop_id = agents_shops.agent_ID AND agents_shops.agent_ID = '$agent_id'";
									$query = mysqli_query($conn_cmd, $cmd);
									
									while($res = mysqli_fetch_assoc($query)){
										$shop_id = $res["shop_ID"];
                    $shop_name = $res["shop_name"];
                    $shop_contact = $res["shop_contact"];
                    $vendor_name = $res["vendor_name"];
                    $vendor_contact = $res["vendor_contact"];
                    $vendor_email = $res["vendor_email"];
                    $shop_location = $res["shop_location"];
                    $shop_category = $res["shop_category"];
										
										echo "<tr>";
											echo "<td>".$shop_id."</td>";
											echo "<td>".$shop_name."</td>";
                      echo "<td>".$shop_contact."</td>";
                      echo "<td>".$vendor_name."</td>";
                      echo "<td>".$vendor_contact."</td>";
                      echo "<td>".$shop_location."</td>";
                      echo "<td>".$vendor_email."</td>";
                      echo "<td>".$shop_category."</td>";
										echo "</tr>";
									}
									
									?>
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Shop Name</th>
                  <th>Shop Contact</th>
                  <th>Vendor Name</th>
                  <th>Vendor Contact</th>
                  <th>Location</th>
                  <th>Email</th>
                  <th>Category</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!--<div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>-->
    <div class="text-center">
      <strong>Copyright &copy; 2020 <a target="_blank" href="#">Kingdom Dynasty Limited</a>.</strong> All rights
    reserved.
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
