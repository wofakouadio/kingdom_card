<?php

	session_start();

	#$agentid = "";
	include('../connection_configuration/conn_config.php');

	// $redirect = "http://localhost/kingdom_project/signup_form/index.php";
		/*function currenturl(){
        	$url = "http://".$_SERVER['HTTP_HOST']."/signup_form/index.php";

        	$validateurl = str_replace("&","&amp",$url);
        	return $validateurl;
      	}
    	$redirect = currenturl();*/

	if(isset($_GET['ref']) ? $_GET['ref'] : ''){

		$agentid = base64_decode($_GET['ref']);

		$fetch_agent = "SELECT * FROM agents_table WHERE agent_ID = '$agentid'";
		$fetch_agent_query = mysqli_query($conn_cmd, $fetch_agent);

		while($fetch_agent_result = mysqli_fetch_array($fetch_agent_query)){

			$agentid = $fetch_agent_result['agent_ID'];
			$agent_name = $fetch_agent_result['name'];
			$agent_username = $fetch_agent_result['username'];

			$_SESSION['agent_id'] = $agentid;
		}

	}else{
		$_SESSION['agent_id'] = "CM8249531076";
	}

	#print_r($_SESSION);

		$msg = "";

	$customer_id = "";
	$name = "";
	$dob = "";
	$gender = "";
	$profession = "";
	$address = "";
	$tel = "";
	$agent = "";
	$picture_path = "";
	$id_card_path = "";
	$mail = "";
	$username = "";
	$password = "";
	$terms_and_conditions = "";
	$userType = "customer";




	if(isset($_POST['submit'])){

		$agent = mysqli_escape_string($conn_cmd, $_POST['f1-referral-agent']);
		$name = mysqli_escape_string($conn_cmd, $_POST['f1-full-name']);
		$dob = mysqli_escape_string($conn_cmd, $_POST['f1-dob']);
		$gender = mysqli_escape_string($conn_cmd, $_POST['f1-gender']);
		$profession = mysqli_escape_string($conn_cmd, $_POST['f1-profession']);
		$address = mysqli_escape_string($conn_cmd, $_POST['f1-address']);
		$tel = mysqli_escape_string($conn_cmd, $_POST['f1-telephone']);
		$mail = mysqli_escape_string($conn_cmd, $_POST['f1-email']);
		$password = mysqli_escape_string($conn_cmd, $_POST['f1-password']);
		$userType = mysqli_escape_string($conn_cmd, "customer");
		$terms_and_conditions = mysqli_escape_string($conn_cmd, $_POST['f1-terms-conditions']);

		$encrypt_password = sha1($password);

		if(isset($terms_and_conditions)){

			$terms_and_conditions = 'Yes';
		}else{
			$terms_and_conditions = 'No';
		}


		//getting username from client name and adding
		//begin
		function getUsernamefromName($name){
			$find = " ";
			$pos = strpos($name,$find);
			$string = substr($name,0,$pos);
			$base = "0123456789";
			$res = (substr(str_shuffle($base),0,5));
			$username = $string.$res;
			return $username;
		}
		$client_username = getUsernamefromName($name);
		//end

		//uploading client picture
		//begin
		$client_picture = $_FILES['f1-passport-picture'];
		$picture_name = $_FILES['f1-passport-picture']['name'];
		$picture_TmpName = $_FILES['f1-passport-picture']['tmp_name'];
		$picture_Type = $_FILES['f1-passport-picture']['type'];
		$picture_Error = $_FILES['f1-passport-picture']['error'];
		$picture_Size = $_FILES['f1-passport-picture']['size'];

		$picture_Ext = explode('.',$picture_name);
		$picture_NewExt = strtolower(end($picture_Ext));
		$Ext_Allowed = array('jpg', 'jpeg', 'png');

		if(in_array($picture_NewExt, $Ext_Allowed)){
			if($picture_Error == 0){
				if($picture_Size < 1000000){
					$picture_NewName = $client_username.".".$picture_NewExt;
					$picture_destination = "../pictures/".$picture_NewName;
					#echo $picture_destination;
					move_uploaded_file($picture_TmpName,$picture_destination);
					#echo "Picture Uploaded Successfully!";
				}else{
					$msg =  "<p class='text-danger text-center'>Your Picture Size is too big!</p>";
				}
			}else{
				$msg =  "<p class='text-danger text-center'>There was an Error uploading your picture!</p>";
			}
		}else{
			$msg =  "<p class='text-danger text-center'>You cannot upload Customer's picture of this type!</p>";
		}

	//end

	//uploading client valid ID
		//begin
		$client_id_picture = $_FILES['f1-valid-id'];
		$client_id_name = $_FILES['f1-valid-id']['name'];
		$client_id_TmpName = $_FILES['f1-valid-id']['tmp_name'];
		$client_id_Type = $_FILES['f1-valid-id']['type'];
		$client_id_Error = $_FILES['f1-valid-id']['error'];
		$client_id_Size = $_FILES['f1-valid-id']['size'];

		$client_id_Ext = explode('.',$client_id_name);
		$client_id_NewExt = strtolower(end($client_id_Ext));
		$Ext_Allowed = array('jpg', 'jpeg', 'png');

		if(in_array($client_id_NewExt, $Ext_Allowed)){
			if($client_id_Error == 0){
				if($client_id_Size < 1000000){
					$client_id_NewName = $client_username.".".$client_id_NewExt;
					$client_id_destination = "../ids/".$client_id_NewName;
					#echo $client_id_destination;
					move_uploaded_file($client_id_TmpName,$client_id_destination);
					#echo "Picture Uploaded Successfully!";
				}else{
					$msg = "<p class='text-danger text-center'>Your Picture Size is too big!</p>";
				}
			}else{
				$msg =  "<p class='text-danger text-center'>There was an Error uploading your picture!</p>";
			}
		}else{
			$msg = "<p class='text-danger text-center'>You cannot upload Customer's valid ID picture of this type!</p>";
		}

	//end

		//generate customer id based on gender and numeric value
		//begin
		if(isset($gender) && ($gender == "Male")){

			function getstring($gender){
				$find = "a";
				$pos = strpos($gender, $find);
				$letter = substr($gender,0,$pos);
				$ID = "C".$letter;
				return $ID;
			}

			$length = 10;
			function generate_numbers($lenght){
				$base = "0123456789";
				$res = substr(str_shuffle($base),0,$lenght);
				return $res;
			}

			$client_id = getstring($gender).generate_numbers($length);


		}

		if(isset($gender) && ($gender == "Female")){

			function getstring($gender){
				$find = "e";
				$pos = strpos($gender, $find);
				$letter = substr($gender,0,$pos);
				$ID = "C".$letter;
				return $ID;
			}

			$length = 10;
			function generate_numbers($lenght){
				$base = "0123456789";
				$res = substr(str_shuffle($base),0,$lenght);
				return $res;
			}

			$client_id = getstring($gender).generate_numbers($length);


		}
		//end
	#print_r($client_picture);

		$chk_client = "SELECT * FROM customers_table WHERE customer_ID = '$client_id' OR customer_username = '$client_username' OR customer_email = '$mail'";
		$exe_chk_client = mysqli_query($conn_cmd,$chk_client);

		$fetch_chk_client = mysqli_num_rows($exe_chk_client);

		if($fetch_chk_client > 0){

			$msg = "<p class='alert alert-warning'>Customer already exists in our database!</p>";

		}
		else{

			$insert_cmd = "INSERT INTO customers_table (customer_ID, customer_name, customer_dob, customer_gender, customer_profession, customer_address, customer_telephone, customer_picture_path, customer_valid_ID_path,customer_email,customer_username, customer_password, referral_agent_id, terms_and_conditions, userType, created_at) VALUES ('$client_id','$name','$dob','$gender','$profession','$address','$tel','$picture_NewName','$client_id_NewName','$mail','$client_username','$encrypt_password', '$agent', '$terms_and_conditions', 'customer', NOW())";

			$insert2_cmd = "INSERT INTO users_pool (pool_id,username,password,userType) VALUES('$client_id','$client_username','$encrypt_password','customer')";

			$exe_insert1_query = mysqli_query($conn_cmd,$insert_cmd);
			$exe_insert2_query = mysqli_query($conn_cmd,$insert2_cmd);
			#$exe_mutli_query = mysqli_multi_query($conn_cmd, $insert_cmd);
			/*$msg =  "<p class='alert alert-success'>Customer registered successfully!</p>";*/

			if($exe_insert1_query AND $exe_insert2_query){
				$_SESSION['Username'] = $client_username;
				$_SESSION['UserType'] = 'customer';
				$_SESSION['id'] = $client_id;
				if($_SESSION['Username'] == $client_username and $_SESSION['UserType'] == 'customer' and $_SESSION['id'] == $client_id){
					$msg =  "<p class='alert alert-success'>Customer registered successfully!</p>";
					$msg = "<p class='alert alert-success'>
							ID: $client_id </br>
							Username: $client_username </br>
							Password: $password </br>
							Redirection to your dashboard in 10s...
						</p>";

					echo "<script>
							//Using setTimeout to execute a function after 10 seconds.
							setTimeout(function () {
   								//Redirect with JavaScript
   								window.location.href= '../customer/';
							}, 10000);
						</script>";
					#header("location:../customer/");
				}
			}
		}
	}
?>


<!DOCTYPE html>
<html lang="en">


<head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Kingdom Card</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <!--<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">-->
				<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
				<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">



        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/img/kd_logo.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/img/kd_logo@144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/kd_logo@114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/kd_logo@72.png">
        <link rel="apple-touch-icon-precomposed" href="assets/img/kd_logo.png">

    </head>

    <body class="bg-dark">
        <!-- Top content -->
        <div class="top-content">
					<div class="container mt-5 px-0">
						<div class="row justify-content-center">
							<div class="col-sm-8 col-sm-offset-1 text bg-primary p-3">
								<h1><strong>Kingdom Card App</strong></h1>
								<small>Register to get instant access</small>
							</div>
						</div>
					</div>
						<?php
							echo $msg;
						?>
					<div class="row justify-content-center">
							<div class="col-sm-10 col-sm-offset-5 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box">
									<form role="form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" class="f1" enctype="multipart/form-data">

											<div class="f1-steps">
													<div class="f1-progress">
															<div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
													</div>
													<div class="f1-step active">
															<div class="f1-step-icon"><i class="fa fa-user"></i></div>
															<p>About</p>
													</div>

													<div class="f1-step">
															<div class="f1-step-icon"><i class="fa fa-book"></i></div>
															<p>Social</p>
													</div>

													<div class="f1-step">
															<div class="f1-step-icon"><i class="fa fa-key"></i></div>
															<p>Account</p>
													</div>
											</div>

											<fieldset>
													<h4>Tell us who you are:</h4>
												<!--<p class="text-danger"><small><b>*Your Referral Agent</b></small></p>-->
												<div class="form-group">
															<label class="sr-only" for="f1-full-name">Agent ID</label>
															<input type="text" name="f1-referral-agent" placeholder="Referral Agent..." class="f1-full-name form-control" id="f1-full-name" value="<?php echo $_SESSION['agent_id'];?>" readonly onkeyup="javascript:this.value=this.value.toUpperCase()">
													</div>
													<p class="text-danger"><small><b>*Your Referral Agent</b><i class="fa fa-arrow-circle-up"></i></small></p>

													<div class="form-group">
															<label class="sr-only" for="f1-full-name">Full Name</label>
															<input type="text" name="f1-full-name" placeholder="Full Name..." class="f1-full-name form-control" id="f1-full-name" onkeyup="javascript:this.value=this.value.toUpperCase()">
													</div>

													<div class="form-group">
															<label class="sr-only" for="f1-dob">Date of Birth</label>
															<input type="text" name="f1-dob" class="f1-dob form-control " id="f1-dob" placeholder="Date of Birth (DD-MM-YYYY)..." required onkeyup="javascript:this.value=this.value.toUpperCase()">
													</div>
													<div class="form-group">
															<label  for="f1-gender">Gender: &nbsp</label>
															<!-- <select name="f1-gender" class="f1-gender form-control" id="f1-gender">
																	<option value="--Select Gender--">--Select Gender--</option>
																	<option value="Male">Male</option>
																	<option value="Female">Female</option>
															</select> -->
															<input type="radio" name="f1-gender" value="Male" class="f1-gender" required> Male &nbsp
															<input type="radio" name="f1-gender" value="Female" class="f1-gender" required> Female
													</div>
													<div class="form-group">
															<label class="sr-only" for="f1-tel">Profession</label>
															<input type="text" name="f1-profession" placeholder="Profession..." class="f1-telephone form-control" id="f1-telephone" onkeyup="javascript:this.value=this.value.toUpperCase()">
													</div>
													<div class="f1-buttons">

															<button type="button" class="btn btn-primary btn-next">Next</button>
													</div>
											</fieldset>


											<fieldset>
															<h4>How to contact You:</h4>
													<div class="form-group">
															<label class="sr-only" for="f1-address">Address for Communication</label>
															<input type="text" name="f1-address" placeholder="Address for Communication..." class="f1-address form-control" id="f1-address" onkeyup="javascript:this.value=this.value.toUpperCase()">
													</div>
													<div class="form-group">
															<label class="sr-only" for="f1-tel">Telephone</label>
															<input type="text" name="f1-telephone" placeholder="Telephone..." class="f1-telephone form-control" id="f1-telephone">
													</div>
													<div class="form-group">Passport Picture: &nbsp <br>
															<label class="sr-only" for="f1-passport-picture">Passport Picture</label>
															<input type="file" name="f1-passport-picture" placeholder="Passport Picture..." class="f1-passport-picture form-control" id="f1-passport-picture">
													</div>
													<div class="form-group">Upload Valid ID: &nbsp <br>
															<label class="sr-only" for="f1-valid-id">Valid ID</label>
															<input type="file" name="f1-valid-id" placeholder="Valid ID..." class="f1-valid-id form-control" id="f1-valid-id">
													</div>
													<div class="f1-buttons">
															<button type="button" class="btn btn-previous">Previous</button>
															<button type="button" class="btn btn-next">Next</button>
													</div>
											</fieldset>

											<fieldset>
													<h4>Create Your Account:</h4>
													<div class="form-group">
															<label class="sr-only" for="f1-username">Email Address</label>
															<input type="text" name="f1-email" placeholder="Email Address..." class="f1-email form-control" id="f1-email">
													</div>
													<div class="form-group">
															<label class="sr-only" for="f1-password">Password</label>
															<input type="password" name="f1-password" placeholder="Password..." class="f1-password form-control" id="f1-password">
													</div>
													<div class="form-group">
														<input type="checkbox" name="f1-terms-conditions" class="form-control" style="width: 50px; float: left;" required> <p>By signing up to Kingdom Card,
															I agree to the <a href="../policies/">Terms <a/> and <a href="../policies/">Conditions</a>.</p>
													</div>
													<div class="f1-buttons">
														<button type="button" class="btn btn-previous">Previous</button>
														<!--<button type="submit" class="btn btn-submit" name="submit">Submit</button>-->
														<input type="submit" name="submit" class="btn btn-submit btn-success" value="Submit">
													</div>
											</fieldset>

									</form>
							</div>
            </div>

        </div>

        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>
    </body>
</html>
