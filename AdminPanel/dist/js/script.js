function autoCalcSetup(){
	
	$('form[name=cart]').jAutoCalc('destroy');
	
	$('form[name=cart] tr[name=line_items]').jAutoCalc({
		keyEventsFire:true, 
		decimalPlaces:2,
		initFire:true,
		thousandOpts: [''],
		decimalOpts: ['.'],
		emptyAsZero: true
	});
	
	$('#grand_total').jAutoCalc({
		thousandOpts: [''],
		decimalOpts: ['.'],
		decimalPlaces:2
	});
	
	$('form[name=cart]').jAutoCalc({
		decimalPlaces:2, 
		decimalOpts: ['.'], 
		thousandOpts: ['']
	});
	
}

autoCalcSetup();

/*

$(document).ready(function(){
	$("#quantity").change(function(){
		$("#hiddenquantity").val($(this).val());
	});
	
	$("#item_total").change(function(){
		$("#hiddenitemtotal").val($(this).val());
	});
});
*/



/*$(document).ready(function(){
	$("#item_sold").each(function(){
		$("#quantity").keydown(function(){
			
			var sum = 0;
			var price = $("#price").val();
			var quantity = $(this).val();
			var item_total = quantity * parseFloat(price);
			$("#item_total").val(item_total);
			
			sum += parseFloat($("#item_total").val(item_total));
			
			$("#grand_total").val(sum);
		});
	});
});*/

