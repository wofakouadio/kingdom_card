<?php

	include('../../../connection_configuration/conn_config.php');
	include('../../phpqrcode/qrlib.php');

	#echo session_status();
	session_start();
	#echo $_SESSION['Username'];
	#echo $_SESSION['UserType'];

	if(!isset($_SESSION['Username']) and !isset($_SESSION['UserType'])){
		header("location:../../../login/");
	}

	$msg = "";
?>





<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="shortcut icon" href="../../../img/kd_logo.png">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>KDL</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Kingdom Dynasty L.</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
    
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="../img/kd_logo@144.png" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $_SESSION['Username']; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../img/kd_logo@144.png" class="img-thumbnail" alt="User Image">
    
                    <p>
                      <?php echo $_SESSION['Username']; ?>
                      <!--<small>Member since Nov. 2012</small>-->
                    </p>
                  </li>
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    
                    <div class="text-center">
                      <a href="../../../logout/" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>
        </nav>
      </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
         <!--<li class="header">MAIN NAVIGATION</li>-->
        <li>
          <a href="../../">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Lists</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="../tables/view_clients.php"><i class="fa fa-users"></i>Clients</a></li>
            <li><a href="../tables/view_shops.php"><i class="fa fa-building-o"></i>Shops</a></li>
						<li><a href="../tables/view_agents_clients.php"><i class="fa fa-home"></i>Agents - Clients</a></li>
            <li><a href="../tables/view_agents_shops.php"><i class="fa fa-home"></i>Agents - Shops</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Registration</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../forms/reg_clients.php"><i class="ion ion-person-add"></i>Clients</a></li>
            <li><a href="../forms/reg_shops.php"><i class="ion ion-bag"></i>Shops</a></li>
						<li><a href="../forms/reg_agents_clients.php"><i class="ion ion-home"></i>Agents - Clients</a></li>
            <li class="active"><a href="../forms/reg_agents_shops.php"><i class="ion ion-home"></i>Agents - shops</a></li>
          </ul>
        </li>
        <li>
          <a href="../tables/pay_day.php">
            <i class="fa fa-money"></i><span>Pay Day</span>
          </a>
        </li>
				<li>
          <a href="../tables/balance.php">
            <i class="fa fa-dollar"></i><span>Balance</span>
          </a>
        </li>
				<li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Commissions</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../tables/agents_clients_commission.php"><i class="fa fa-money"></i><span>Agents - Clients<span></a></li>
            <li><a href="../tables/agents_shops_commission.php"><i class="fa fa-money"></i><span>Agents - Shops<span></a></li>
          </ul>
        </li>
				<li>
          <a href="../tables/generate_qrcode.php">
            <i class="fa fa-qrcode"></i><span>Generate Customer QR-Code</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Agent - Shops Registration Form
      </h1>
			
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Registration</a></li>
        <li class="active">Agents - Shops</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php $_SERVER['PHP_SELF']?>" method="post" enctype="multipart/form-data">
              <div class="box-body col-md-6">
                <div class="form-group">
                    <label for="exampleInputText1">Name</label>
                    <input type="text" class="form-control" name="agentname" id="exampleInputText1 inputError" placeholder="Full Name..." required onkeyup="javascript:this.value=this.value.toUpperCase()">
                </div>
                <div class="form-group">
                    <label for="agentgender">Gender :</label>&nbsp;
                    <input type="radio" name="agentgender" value="Male" class="custom-radio" required>&nbsp; Male &nbsp;
                    <input type="radio" name="agentgender" value="Female" class="custom-radio" required> &nbsp; Female 
                </div>
                <div class="form-group">
                    <label for="exampleInputText3">Profession</label>
                    <input type="text" class="form-control" name="agentprofession" id="exampleInputTex2t" placeholder="Profession..." required onkeyup="javascript:this.value=this.value.toUpperCase()">
                </div>
                <div class="form-group">
                    <label for="exampleInputText4">Address</label>
                    <input type="text" class="form-control" name="agentaddress" id="exampleInputText3" placeholder="Address..." required onkeyup="javascript:this.value=this.value.toUpperCase()">
                </div>
                <div class="form-group">
                    <label for="exampleInputText5">Telephone Number</label>
                    <input type="text" class="form-control" name="agenttel" id="exampleInputText4" placeholder="Telephone Number..." required onkeyup="javascript:this.value=this.value.toUpperCase()">
                </div>
              </div>
              <!-- /.box-body -->
							
							<div class="box-body col-md-6">
                <div class="form-group">
                    <label for="exampleInputFile1">Upload Picture</label>
                    <input type="file" id="exampleInputFile2" name="agentpicpath" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile2">Upload Valid ID</label>
                    <input type="file" id="exampleInputFile2"  name="agentidpath" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" name="agentemail" id="exampleInputEmail1" placeholder="Enter email..." required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" name="agentpassword" id="exampleInputPassword1" placeholder="Password..." required>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-block" name="register_agent">Submit</button>
								
						 <?php

							$agent_id = "";
							$name = "";
							$gender = "";
							$profession = "";
							$address = "";
							$tel = "";
							$picture_path = "";
							$id_card_path = "";
							$mail = "";
							$agent_username = "";
							$password = "";
							$userType = "agent";
							$date = "";



							if(isset($_POST['register_agent'])){

								$name = $_POST['agentname'];
								#$dob = $_POST['agentdob'];
								$gender = $_POST['agentgender'];
								$profession = $_POST['agentprofession'];
								$address = $_POST['agentaddress'];
								$tel = $_POST['agenttel'];
								$mail = $_POST['agentemail'];
								$password =$_POST['agentpassword'];
                $encrypt_password = sha1($password);
								$userType = "agent-shop";
								$date = date('D-M-Y');


								//getting username from client name and adding
								//begin
								function getUsernamefromName($name){
									$find = " ";
									$pos = strpos($name,$find);
									$string = substr($name,0,$pos);
									$base = "0123456789";
									$res = (substr(str_shuffle($base),0,5));
									$username = $string.$res;
									return $username;
								}
								$agent_username = getUsernamefromName($name);
								//end

								//uploading client picture
								//begin
								$agent_picture = $_FILES['agentpicpath'];
								$picture_name = $_FILES['agentpicpath']['name'];
								$picture_TmpName = $_FILES['agentpicpath']['tmp_name'];
								$picture_Type = $_FILES['agentpicpath']['type'];
								$picture_Error = $_FILES['agentpicpath']['error'];
								$picture_Size = $_FILES['agentpicpath']['size'];

								$picture_Ext = explode('.',$picture_name);
								$picture_NewExt = strtolower(end($picture_Ext));
								$Ext_Allowed = array('jpg', 'jpeg', 'png');

								if(in_array($picture_NewExt, $Ext_Allowed)){
									if($picture_Error == 0){
										if($picture_Size < 1000000){
											$picture_NewName = $agent_username.".".$picture_NewExt;
											$picture_destination = "../../../pictures/".$picture_NewName;
											#echo $picture_destination;
											move_uploaded_file($picture_TmpName,$picture_destination);
											#echo "Picture Uploaded Successfully!";
										}else{
											echo "<p class='info-box bg-red'>Your Picture Size is too big!</p>";
										}
									}else{
										echo "<p class='info-box bg-red'>There was an Error uploading your picture!</p>";
									}
								}else{
									echo "<p class='alert alert-danger'>You cannot upload Customer's picture of this type!</p>";
								}

							//end

							//uploading client valid ID
								//begin
								$agent_id_picture = $_FILES['agentidpath'];
								$agent_id_name = $_FILES['agentidpath']['name'];
								$agent_id_TmpName = $_FILES['agentidpath']['tmp_name'];
								$agent_id_Type = $_FILES['agentidpath']['type'];
								$agent_id_Error = $_FILES['agentidpath']['error'];
								$agent_id_Size = $_FILES['agentidpath']['size'];

								$agent_id_Ext = explode('.',$agent_id_name);
								$agent_id_NewExt = strtolower(end($agent_id_Ext));
								$Ext_Allowed = array('jpg', 'jpeg', 'png');

								if(in_array($agent_id_NewExt, $Ext_Allowed)){
									if($agent_id_Error == 0){
										if($agent_id_Size < 1000000){
											$agent_id_NewName = $agent_username.".".$agent_id_NewExt;
											$agent_id_destination = "../../../ids/".$agent_id_NewName;
											#echo $agent_id_destination;
											move_uploaded_file($agent_id_TmpName,$agent_id_destination);
											#echo "Picture Uploaded Successfully!";
										}else{
											echo "<p class='alert alert-danger'>Your Picture Size is too big!</p>";
										}
									}else{
										echo "<p class='alert alert-danger'>There was an Error uploading your picture!</p>";
									}
								}else{
									echo "<p class='alert alert-danger'>You cannot upload Customer's valid ID picture of this type!</p>";
								}

							//end

								//generate customer id based on gender and numeric value
								//begin
								if(isset($gender) && ($gender == "Male")){

									function getstring($gender){
										$find = "a";
										$pos = strpos($gender, $find);
										$letter = substr($gender,0,$pos);
										$ID = "C".$letter;
										return $ID;
									}

									$length = 10;
									function generate_numbers($lenght){
										$base = "0123456789";
										$res = substr(str_shuffle($base),0,$lenght);
										return $res;
									}

									$agent_id = getstring($gender).generate_numbers($length);


								}

								if(isset($gender) && ($gender == "Female")){

									function getstring($gender){
										$find = "e";
										$pos = strpos($gender, $find);
										$letter = substr($gender,0,$pos);
										$ID = "C".$letter;
										return $ID;
									}

									$length = 10;
									function generate_numbers($lenght){
										$base = "0123456789";
										$res = substr(str_shuffle($base),0,$lenght);
										return $res;
									}

									$agent_id = getstring($gender).generate_numbers($length);


								}
								
								// $url = "http://localhost/kingdom_project/signup_form/index.php";
											
								// $referral_link = $url."?ref=".$agent_id;
								//end
							#print_r($client_picture);

								$chk_client = "SELECT * FROM agents_shops WHERE agent_ID = '$agent_id' AND username = '$agent_username'";
								$exe_chk_client = mysqli_query($conn_cmd,$chk_client);

								$fetch_chk_client = mysqli_num_rows($exe_chk_client);

								if($fetch_chk_client > 0){

									echo "<p class='alert alert-warning text-center'>Agent already exists in our database!</p>";

								}
								else{

							/*		echo $agent_id;
									echo $name;
									echo $gender;
									echo $profession;
									echo $address;
									echo $tel;
									echo $picture_destination;
									echo $agent_id_destination;
									echo $mail;
									echo $agent_username;
									echo $password;
									echo $userType;
									echo $date;*/
									
									$insert_agent = "INSERT INTO agents_shops(agent_ID, name ,gender ,profession ,address ,contact ,agent_picture ,agent_valid_id ,email ,username ,password ,user_type ,created_at) VALUES('$agent_id', '$name', '$gender', '$profession', '$address', '$tel', '$picture_NewName', '$agent_id_NewName', '$mail', '$agent_username', '$encrypt_password', '$userType', NOW())";
									
									$insert_user_pool = "INSERT INTO users_pool(pool_id, username, password, userType) VALUES('$agent_id', '$agent_username', '$encrypt_password', '$userType')";
									
									// $insert_referral_link = "INSERT INTO referrals_table(agent_ID, referral_link) VALUES('$agent_id', '$referral_link')";
									
									$exe_insert1_query = mysqli_query($conn_cmd,$insert_agent);
									
									$exe_insert2_query = mysqli_query($conn_cmd,$insert_user_pool);
	
									// $exe_insert3_query = mysqli_query($conn_cmd, $insert_referral_link);
									
									if(!$exe_insert1_query && !$exe_insert2_query){
										echo "</br><p class='alert alert-danger text-center'><b>Agent registration failed!!!</b></p>";
										mysqli_error($conn_cmd);
									}else{
										echo "</br><h3 class='alert alert-success text-center'>
                                  <b>Agent registration done successfully!!!</b><br>
                                  Agent ID: ".$agent_id."<br>
                                  Agent Username: ".$agent_username."<br>
                                  Agent Password: ".$password."
                                </h3>";
										mysqli_close($conn_cmd);
									}
								}
							}




						?>
              </div>
            </form>
          </div>
        </div>
		  
 

        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
        <div class="text-center">
          <strong>Copyright &copy; 2020 <a target="_blank" href="#">Kingdom Dynasty Limited</a>.</strong> All rights
        reserved.
        </div>
      </footer>
    </div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
