<?php

	include('../../../connection_configuration/conn_config.php');

	#echo session_status();
	session_start();
	#echo $_SESSION['Username'];
	#echo $_SESSION['UserType'];

	if(!isset($_SESSION['Username']) and !isset($_SESSION['UserType'])){
		header("location:../../../login/");
	}


?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!--ajax jquery script-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="shortcut icon" href="../../../img/kd_logo.png">
	
	<script>
		$(document).ready(function(e){
			
			var html = '<tr><td>Item Sold<input type="text" name="item_name[]" id="item_name" class="form-control item_list"></td><td>U.P<input type="text" name="item_unit_price[]" id="item_unit_price" class="form-control up_list"></td><td><button type="button" class="btn btn-danger btn-xs btn_remove" id="btn_remove" name="btn_remove"><span class="fa fa-minus"></span></button></td></tr>';
			
			
			
			$('#add').click(function(e){
				$('#tableitemsold').append(html);
				
			});
			
			$('#tableitemsold').on('click','#btn_remove',function(e){
				$(this).closest('tr').remove();
				
			});
			
			$('#tableitemsold').on('dblclick','#item_name',function(e){
				$(this).val($('#item_name').val());
			});
			
			$('#tableitemsold').on('dblclick','#item_unit_price',function(e){
				$(this).val($('#item_unit_price').val());
			});
			
		/*	$('#register_shop').click(function(){
				$.ajax({
					url:"../forms/fetch.php",
					method:"POST",
					data:$('#insert_shop_form').serialize(),
					success:function(data){
						alert(data);
						$('#insert_shop_form')[0].reset();
					}
				});
			});*/
			
		});
	</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>KDL</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Kingdom Dynasty L.</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
    
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="../img/kd_logo@144.png" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $_SESSION['Username'];?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../img/kd_logo@144.png" class="img-thumbnail" alt="User Image">
    
                    <p>
                      <?php echo $_SESSION['Username'];?>
                     
                    </p>
                  </li>
      
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    
                    <div class="text-center">
                      <a href="../../../logout/" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>
        </nav>
      </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
         <!--<li class="header">MAIN NAVIGATION</li>-->
        <li>
          <a href="../../">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            
          </a>
          
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Lists</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="../tables/view_clients.php"><i class="fa fa-users"></i>Clients</a></li>
            <li><a href="../tables/view_shops.php"><i class="fa fa-building-o"></i>Shops</a></li>
						<li><a href="../tables/view_agents_clients.php"><i class="fa fa-home"></i>Agents - Clients</a></li>
            <li><a href="../tables/view_agents_shops.php"><i class="fa fa-home"></i>Agents - Shops</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Registration</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../forms/reg_clients.php"><i class="ion ion-person-add"></i>Clients</a></li>
            <li class="active"><a href="../forms/reg_shops.php"><i class="ion ion-bag"></i>Shops</a></li>
						<li><a href="../forms/reg_agents_clients.php"><i class="ion ion-home"></i>Agents - Clients</a></li>
            <li><a href="../forms/reg_agents_shops.php"><i class="ion ion-home"></i>Agents - shops</a></li>
          </ul>
        </li>
        <li>
          <a href="../tables/pay_day.php">
            <i class="fa fa-money"></i><span>Pay Day</span>
          </a>
        </li>
				<li>
          <a href="../tables/balance.php">
            <i class="fa fa-dollar"></i><span>Balance</span>
          </a>
        </li>
				<li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Commissions</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../tables/agents_clients_commission.php"><i class="fa fa-money"></i><span>Agents - Clients<span></a></li>
            <li><a href="../tables/agents_shops_commission.php"><i class="fa fa-money"></i><span>Agents - Shops<span></a></li>
          </ul>
        </li>
				<li >
          <a href="../tables/generate_qrcode.php">
            <i class="fa fa-qrcode"></i><span>Generate Customer QR-Code</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shop Registration Form
      </h1>
			<p><?php echo $msg;?></p>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Registration</a></li>
        <li class="active">Shops</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data" id="insert_shop_form">
              <div class="box-body col-md-6">
                <div class="form-group">
                    <label for="exampleInputText1">Shop's Name</label>
                    <input type="text" class="form-control" id="exampleInputText1" placeholder="Shop's Name..." name="shop_name" required onkeyup="javascript:this.value=this.value.toUpperCase()">
                </div>
                <div class="form-group">
                    <label for="exampleInputText2">Vendor's Name</label>
                    <input type="text" class="form-control" data-mask placeholder="Vendor's Name" name="vendor_name" required onkeyup="javascript:this.value=this.value.toUpperCase()">
                </div>
                <div class="form-group">
                    <label>Shop's Location or Address</label>
                    <textarea class="form-control" rows="3" placeholder="Shop's Location or Address" name="shop_location" required onkeyup="javascript:this.value=this.value.toUpperCase()"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputText5">Shop's Contact</label>
                    <input type="text" class="form-control" id="exampleInputText4" placeholder="Shop's Contact..." name="shop_contact" required onkeyup="javascript:this.value=this.value.toUpperCase()">
                </div>
                <div class="form-group">
                    <label for="exampleInputText5">Vendor's Contact</label>
                    <input type="text" class="form-control" id="exampleInputText4" placeholder="Vendor's Contact..." name="vendor_contact" required onkeyup="javascript:this.value=this.value.toUpperCase()">
                </div>
              </div>
							
							 <div class="box-body col-md-6">
                
                <!-- <div class="form-group">
                                  <table class="table table-responsive table-striped tableitemsold" id="tableitemsold" name="tableitemsold">
                                    <div>
                                      <tr>
                                        <td>
                                          Item Sold
                                          <input type="text" name="item_name[]" id="item_name" class="form-control item_list" required>
                                        </td>
                                        <td>
                                          Unit Price
                                          <input type="text" name="item_unit_price[]" id="item_unit_price" class="form-control up_list" required>
                                        </td>
                                        <td><button type="button" class="btn btn-success btn-xs add" id="add" name="add"><span class="fa fa-plus"></span></button></td>
                                      </tr>
                                    </div>
                                  </table>
                                </div> -->
                <div class="form-group">
                    <label>Category</label>
                    <select class="form-control select" style="width: 100%;" name="shop_category" required>
                        <option value="-1">Select Category</option>
                        <option value="Hair Salon">Hair Salon</option>
                        <option value="Beautician Center">Beautician Center</option>
                        <option value="BaberShop">BaberShop</option>
                        <option value="Supermarket">Supermarket</option>
                        <option value="Shopping Mall">Shopping Mall</option>
                        <option value="Provision Store">Provision Store</option>
                        <option value="Boutique">Boutique</option>
                        <option value="Fashion">Fashion</option>
                        <option value="Jewelry">Jewelry</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Discount</label>
                    <select class="form-control" style="width: 100%;" name="shop_discount" required>
                        <option value="-1">Select Discount</option>
                        <option value="2%">2% Discount</option>
                        <option value="5%">5% Discount</option>
                        <option value="10%">10% Discount</option>
                        <option value="12%">12% Discount</option>
                        <option value="15%">15% Discount</option>
                        <option value="20%">20% Discount</option>
                        <option value="25%">25% Discount</option>
                    </select>
                </div>

                <div class="form-group" style="width: 50%; float: left;">
                    <label for="exampleInputFile1">Upload Picture</label>
                    <input type="file" id="exampleInputFile2" name="vendorpicpath" class="form-control" required>
                </div>
                <div class="form-group" style="width: 50%; float: right;">
                    <label for="exampleInputFile2">Upload Valid ID</label>
                    <input type="file" id="exampleInputFile2" name="vendoridpath" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email..." name="vendor_mail" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password..." name="vendor_password" required>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <!--<button type="submit" class="btn btn-primary" name="signin" value>Submit</button>-->
								<input type="submit" id="register_shop" name="register_shop" value="Submit" class="btn btn-primary register_shop">
              </div>
            </form>
         
         
						<?php
	
							//declaring different variables to hold data
		
									$shop_id = "";
									$vendor_name = "";
									$shop_name = "";
									$shop_location = "";
									$shop_contact = "";
									$vendor_contact = "";
									$shop_category = "";
									$shop_discount = "";
									$item_sold = "";
									$up = "";
									$vendor_pic_path = "";
									$vendor_validID_path = "";
									$vendor_mail = "";
									$vendor_username = "";
									$vendor_password = "";
									$userType = "shop";
									
							//if the button submit is clicked
									if(isset($_POST['register_shop'])){
										
										$shop_name = mysqli_escape_string($conn_cmd, $_POST['shop_name']);
										$vendor_name = mysqli_escape_string($conn_cmd, $_POST['vendor_name']);
										$shop_location = $_POST['shop_location'];
										$shop_contact = $_POST['shop_contact'];
										$vendor_contact = $_POST['vendor_contact'];
										$shop_category = $_POST['shop_category'];
										#$item_sold = $_POST['item_name'];
										#$up = $_POST['item_unit_price'];
										$shop_discount = $_POST['shop_discount'];
										#$vendor_pic_path = $_POST['vendorpicpath'];
										#$vendor_validID_path = $_POST['vendoridpath'];
										$vendor_mail = $_POST['vendor_mail'];
										$vendor_password = sha1($_POST['vendor_password']);
										$userType = "shop";
										
										
										
										//getting username from shop name and adding
										//begin
										function getUsernamefromName($shop_name){
											$find = " ";
											$pos = strpos($shop_name,$find);
											$string = substr($shop_name,0,$pos);
											$base = "0123456789";
											$res = (substr(str_shuffle($base),0,5));
											$username = $string.$res;
											return $username;
										}
										$vendor_username = getUsernamefromName($shop_name);
										//end

										//uploading vendor picture
										//begin
										$vendor_pic_path = $_FILES['vendorpicpath'];
										$picture_name = $_FILES['vendorpicpath']['name'];
										$picture_TmpName = $_FILES['vendorpicpath']['tmp_name'];
										$picture_Type = $_FILES['vendorpicpath']['type'];
										$picture_Error = $_FILES['vendorpicpath']['error'];
										$picture_Size = $_FILES['vendorpicpath']['size'];

										$picture_Ext = explode('.',$picture_name);
										$picture_NewExt = strtolower(end($picture_Ext));
										$Ext_Allowed = array('jpg', 'jpeg', 'png');

										if(in_array($picture_NewExt, $Ext_Allowed)){
											if($picture_Error == 0){
												if($picture_Size < 1000000){
													$picture_NewName = $vendor_username.".".$picture_NewExt;
													$picture_destination = "../../../pictures/".$picture_NewName;
													#echo $picture_destination;
													#move_uploaded_file($picture_TmpName,$picture_destination);
													#echo "Picture Uploaded Successfully!";
												}else{
													echo "<p class='info-box bg-red'>Your Picture Size is too big!</p>";
												}
											}else{
												echo "<p class='info-box bg-red'>There was an Error uploading your picture!</p>";
											}
										}else{
											echo "<p class='alert alert-danger'>You cannot upload Customer's picture of this type!</p>";
										}

									//end

									//uploading vendor valid ID
										//begin
										$vendor_validID_path = $_FILES['vendoridpath'];
										$vendor_id_name = $_FILES['vendoridpath']['name'];
										$vendor_id_TmpName = $_FILES['vendoridpath']['tmp_name'];
										$vendor_id_Type = $_FILES['vendoridpath']['type'];
										$vendor_id_Error = $_FILES['vendoridpath']['error'];
										$vendor_id_Size = $_FILES['vendoridpath']['size'];

										$vendor_id_Ext = explode('.',$vendor_id_name);
										$vendor_id_NewExt = strtolower(end($vendor_id_Ext));
										$Ext_Allowed = array('jpg', 'jpeg', 'png');

										if(in_array($vendor_id_NewExt, $Ext_Allowed)){
											if($vendor_id_Error == 0){
												if($vendor_id_Size < 1000000){
													$vendor_id_NewName = $vendor_username.".".$vendor_id_NewExt;
													$vendor_id_destination = "../../../ids/".$vendor_id_NewName;
													#echo $client_id_destination;
													#move_uploaded_file($vendor_id_TmpName,$vendor_id_destination);
													#echo "Picture Uploaded Successfully!";
												}else{
													echo "<p class='alert alert-danger'>Your Picture Size is too big!</p>";
												}
											}else{
												echo "<p class='alert alert-danger'>There was an Error uploading your picture!</p>";
											}
										}else{
											echo "<p class='alert alert-danger'>You cannot upload Customer's valid ID picture of this type!</p>";
										}

									//end
										
										//generate shop id based on shop category and numeric value
										//begin
										if(isset($shop_category) && ($shop_category == "Hair Salon")){

											function getstring($shop_category){
												$find = "air Salon";
												$pos = strpos($shop_category, $find);
												$letter = substr($shop_category,0,$pos);
												$ID = "S".$letter;
												return $ID;
											}

											$length = 5;
											function generate_numbers($lenght){
												$base = "0123456789";
												$res = substr(str_shuffle($base),0,$lenght);
												return $res;
											}

											$shop_id = getstring($shop_category).generate_numbers($length);


										}


                    if(isset($shop_category) && ($shop_category == "Beautician Center")){

                      function getstring($shop_category){
                        $find = "eautician Center";
                        $pos = strpos($shop_category, $find);
                        $letter = substr($shop_category,0,$pos);
                        $ID = "S".$letter;
                        return $ID;
                      }

                      $length = 5;
                      function generate_numbers($lenght){
                        $base = "0123456789";
                        $res = substr(str_shuffle($base),0,$lenght);
                        return $res;
                      }

                      $shop_id = getstring($shop_category).generate_numbers($length);


                    }

										
										if(isset($shop_category) && ($shop_category == "BaberShop")){
											
											function getstring($shop_category){
												$find = "aberShop";
												$pos = strpos($shop_category, $find);
												$letter = substr($shop_category,0,$pos);
												$ID = "S".$letter;
												return $ID;
											}

											$length = 5;
											function generate_numbers($lenght){
												$base = "0123456789";
												$res = substr(str_shuffle($base),0,$lenght);
												return $res;
											}

											$shop_id = getstring($shop_category).generate_numbers($length);


										}
										
										if(isset($shop_category) && ($shop_category == "Supermarket")){
											
											function getstring($shop_category){
												$find = "upermarket";
												$pos = strpos($shop_category, $find);
												$letter = substr($shop_category,0,$pos);
												$ID = "S".$letter;
												return $ID;
											}

											$length = 5;
											function generate_numbers($lenght){
												$base = "0123456789";
												$res = substr(str_shuffle($base),0,$lenght);
												return $res;
											}

											$shop_id = getstring($shop_category).generate_numbers($length);


										}
										
										if(isset($shop_category) && ($shop_category == "Boutique")){
											
											function getstring($shop_category){
												$find = "outique";
												$pos = strpos($shop_category, $find);
												$letter = substr($shop_category,0,$pos);
												$ID = "S".$letter;
												return $ID;
											}

											$length = 5;
											function generate_numbers($lenght){
												$base = "0123456789";
												$res = substr(str_shuffle($base),0,$lenght);
												return $res;
											}

											$shop_id = getstring($shop_category).generate_numbers($length);


										}
										
										if(isset($shop_category) && ($shop_category == "Fashion")){
											
											function getstring($shop_category){
												$find = "ashion";
												$pos = strpos($shop_category, $find);
												$letter = substr($shop_category,0,$pos);
												$ID = "S".$letter;
												return $ID;
											}

											$length = 5;
											function generate_numbers($lenght){
												$base = "0123456789";
												$res = substr(str_shuffle($base),0,$lenght);
												return $res;
											}

											$shop_id = getstring($shop_category).generate_numbers($length);


										}
										
										if(isset($shop_category) && ($shop_category == "Jewelry")){
											
											function getstring($shop_category){
												$find = "ewelry";
												$pos = strpos($shop_category, $find);
												$letter = substr($shop_category,0,$pos);
												$ID = "S".$letter;
												#$ID = "S".$letter;
												return $ID;
											}

											$length = 5;
											function generate_numbers($lenght){
												$base = "0123456789";
												$res = substr(str_shuffle($base),0,$lenght);
												return $res;
											}

											$shop_id = getstring($shop_category).generate_numbers($length);
										}

                    if(isset($shop_category) && ($shop_category == "Shopping Mall")){
                      
                      function getstring($shop_category){
                        $find = "hopping Mall";
                        $pos = strpos($shop_category, $find);
                        $letter = substr($shop_category,0,$pos);
                        $ID = "S".$letter;
                        #$ID = "S".$letter;
                        return $ID;
                      }

                      $length = 5;
                      function generate_numbers($lenght){
                        $base = "0123456789";
                        $res = substr(str_shuffle($base),0,$lenght);
                        return $res;
                      }

                      $shop_id = getstring($shop_category).generate_numbers($length);
                    }

                    if(isset($shop_category) && ($shop_category == "Provision Store")){
                      
                      function getstring($shop_category){
                        $find = "rovision Store";
                        $pos = strpos($shop_category, $find);
                        $letter = substr($shop_category,0,$pos);
                        $ID = "S".$letter;
                        #$ID = "S".$letter;
                        return $ID;
                      }

                      $length = 5;
                      function generate_numbers($lenght){
                        $base = "0123456789";
                        $res = substr(str_shuffle($base),0,$lenght);
                        return $res;
                      }

                      $shop_id = getstring($shop_category).generate_numbers($length);
                    }
										
										#echo $shop_id;
										
										$chk_shop = "SELECT * FROM shops_table WHERE shop_ID='$shop_id' AND vendor_username='$vendor_username'";
										
										$chk_shop_query = mysqli_query($conn_cmd, $chk_shop);
										
										$fetch_chk_shop = mysqli_num_rows($chk_shop_query);
										
										if($fetch_chk_shop > 0){
											
											echo "<p class='alert alert-warning'>Shop already exists in our database!</p>";
											
										}
										else{
											
											/*foreach ($item_sold as $key => $value){
											
											$cmd = "INSERT INTO category_table (shop_ID, shop_category, shop_item_sold, unit_price) VALUES ('$shop_id','$shop_category','".$value."','".$up[$key]."')";
											
											$query = mysqli_query($conn_cmd,$cmd);
											
											
										  }*/
											
											$insert_cmd = "INSERT INTO shops_table (shop_ID, shop_name, vendor_name, shop_location, shop_contact, vendor_contact, shop_category, shop_discount, vendor_picture_path, vendor_ID_path, vendor_email, vendor_username, vendor_password, userType,created_at) VALUES ('$shop_id','$shop_name','$vendor_name','$shop_location','$shop_contact','$vendor_contact','$shop_category','$shop_discount','$picture_NewName','$vendor_id_NewName','$vendor_mail','$vendor_username','$vendor_password','shop', NOW());
											
											INSERT INTO users_pool (pool_id,username,password,userType) VALUES('$shop_id','$vendor_username','$vendor_password','shop')";
											
											if($exe_multi_query = mysqli_multi_query($conn_cmd,$insert_cmd)){
												
												echo "<p class='alert alert-success'>Shop registered successfully!!!</p>";
												echo "<p class='alert alert-info'>Shop Username: ".$vendor_username.", Shop ID: ".$shop_id."</p>";
											}
											
											
										}
										

						}
									
									
						?>
				 </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
       
        <div class="text-center">
          <strong>Copyright &copy; 2020 <a target="_blank" href="#">Kingdom Dynasty Limited</a>.</strong> All rights
        reserved.
        </div>
      </footer>
    </div>
<!-- ./wrapper -->

<!--ajax jquery script-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="../../bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
	
	
</body>
</html>


