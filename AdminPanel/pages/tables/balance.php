<?php

	include('../../../connection_configuration/conn_config.php');

	#echo session_status();
	session_start();
	#echo $_SESSION['Username'];
	#echo $_SESSION['UserType'];

	if(!isset($_SESSION['Username']) and !isset($_SESSION['UserType'])){
		header("location:../../../login/");
	}


?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="shortcut icon" href="../../../img/kd_logo.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>KDL</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Kingdom Dynasty L.</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../img/kd_logo@144.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['Username'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../img/kd_logo@144.png" class="img-thumbnail" alt="User Image">

                <p>
                  <?php echo $_SESSION['Username'];?>
                 
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
              
                <div class="text-center">
                  <a href="../../../logout/" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
         
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
         <!--<li class="header">MAIN NAVIGATION</li>-->
        <li>
          <a href="../../">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Lists</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li ><a href="../tables/view_clients.php"><i class="fa fa-users"></i>Clients</a></li>
            <li><a href="../tables/view_shops.php"><i class="fa fa-building-o"></i>Shops</a></li>
						<li><a href="../tables/view_agents_clients.php"><i class="fa fa-home"></i>Agents - Clients</a></li>
            <li><a href="../tables/view_agents_shops.php"><i class="fa fa-home"></i>Agents - Shops</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Registration</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../forms/reg_clients.php"><i class="ion ion-person-add"></i> Clients</a></li>
            <li><a href="../forms/reg_shops.php"><i class="ion ion-bag"></i> Shops</a></li>
						<li><a href="../forms/reg_agents_clients.php"><i class="ion ion-home"></i>Agents - Clients</a></li>
            <li><a href="../forms/reg_agents_shops.php"><i class="ion ion-home"></i>Agents - shops</a></li>
          </ul>
        </li>
        <li>
          <a href="../tables/pay_day.php">
            <i class="fa fa-money"></i><span>Pay Day</span>
          </a>
        </li>
		<li class="active">
          <a href="../tables/balance.php">
            <i class="fa fa-dollar"></i><span>Balance</span>
          </a>
        </li>
				<li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Commissions</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../tables/agents_clients_commission.php"><i class="fa fa-money"></i><span>Agents - Clients<span></a></li>
            <li><a href="../tables/agents_shops_commission.php"><i class="fa fa-money"></i><span>Agents - Shops<span></a></li>
          </ul>
        </li>
				<li>
          <a href="../tables/generate_qrcode.php">
            <i class="fa fa-qrcode"></i><span>Generate Customer QR-Code</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Balance Table
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Balance</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
									<th>#</th>
                  <th>Shop's Name</th>
                  <th>Vendor's Name</th>
                  <th>Location</th>
                  <th>Contact</th>
				  				<th>Credit</th>
                  <th>Status</th>
                  <th>Agent Name</th>
                  <th>Agent Contact</th>
				  				<th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
									
									$cmd = "SELECT purchases_table.id, purchases_table.purchase_ID, shops_table.shop_ID, shops_table.shop_name, shops_table.vendor_name, shops_table.shop_location, shops_table.shop_contact, shops_table.vendor_contact, purchases_table.balance, purchases_table.status, shops_table.agent_shop_id, agents_shops.agent_ID, agents_shops.name, agents_shops.contact FROM shops_table JOIN purchases_table JOIN balance_table JOIN agents_shops WHERE purchases_table.purchase_ID = balance_table.purchase_ID AND shops_table.shop_ID = purchases_table.shop_ID AND shops_table.agent_shop_id = agents_shops.agent_ID";
									
									$query = mysqli_query($conn_cmd,$cmd);
									
									while($res= mysqli_fetch_array($query)){
										$numberid = $res['id'];
										$pid = $res['purchase_ID'];
										$id = $res['shop_ID'];
										$name = $res['shop_name'];
										$vendorname = $res['vendor_name'];
										$loc = $res['shop_location'];
										$shopcontact = $res['shop_contact']."/".$res['vendor_contact'];
										$balance = $res['balance'];
										$status = $res['status'];
                    $agent_shop_id = $res["agent_ID"];
                    $agent_contact = $res["contact"];
                    $agent_name = $res["name"];
										
										echo "<tr><form method='post'>";
											echo "<td>".$numberid."<input type='hidden' name='numberid' value=".$numberid."></td>";
											echo "<td>".$name."</td>";
											echo "<td>".$vendorname."</td>";
											echo "<td>".$loc."</td>";
											echo "<td>".$shopcontact."</td>";
											echo "<td>".$balance."<input type='hidden' name='pid' value=".$pid."></td>";
											echo "<td>".$status."</td>";
                      echo "<td>".$agent_name."<input type='hidden' name='agent_id' value=".$agent_shop_id."></td>";
                      echo "<td>".$agent_contact."</td>";
											echo "<td><input type = 'submit' name = 'paidbtn' value = 'Update' class = 'btn btn-info'></td>";
										echo "</form></tr>";
									}
									
									if(isset($_POST['paidbtn'])){
										
										$id = $_POST['numberid'];
										$pid = $_POST['pid'];
										$exe = "UPDATE `purchases_table` SET `status` = 'Paid' WHERE purchases_table.purchase_ID = '$pid'";
										$exe2 = "UPDATE `balance_table` SET `status` = 'Paid' WHERE balance_table.purchase_ID = '$pid'";
										$exe_query = mysqli_query($conn_cmd,$exe);
										$exe2_query = mysqli_query($conn_cmd,$exe2);
										if($exe_query && $exe2_query){
											echo "<p class='alert alert-success'> Update done successfully...</p>";
										}
										
									}
									
								?>
                </tbody>
                <tfoot>
                <tr>
									<th>#</th>
                  <th>Shop's Name</th>
                  <th>Vendor's Name</th>
                  <th>Location</th>
                  <th>Contact</th>
									<th>Credit</th>
									<th>Status</th>
                  <th>Agent Name</th>
                  <th>Agent Contact</th>
									<th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!--<div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>-->
    <div class="text-center">
      <strong>Copyright &copy; 2020 <a target="_blank" href="#">Kingdom Dynasty Limited</a>.</strong> All rights
    reserved.
    </div>
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
  })
</script>
</body>
</html>

